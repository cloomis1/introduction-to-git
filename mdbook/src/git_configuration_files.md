# Git Configuration Files

## README
The README file is the first file that a person visiting your repository should review. It can contain information on how to navigate the repository, the purpose of the repository, or whatever else you deem necessary for someone to know who is visiting your repository. The README file in Gitlab is configured using markdown and has a file extension of .md. When creating a new repository you will have the option to automatically add a blank **README**.md file that you can then edit afterwards. If you forget to add the README file to your repository at creation that's fine. Just create a new file at the root of your repository and name it **README**.md.

## .gitignore

The .gitignore file is simply a file that you use to specify which files you intentionally want to leave untracked for a repository. In other words, you want git to ignore these files. These gitignore files support all kinds of pattern formatting, which you can find in the documentation link below. 

In order to add a .gitignore file to your repository in Gitlab, simply go to add a new file, and select the .gitignore template. You can even choose to specify a Language or Global template to get you started on common things that are ignored within a repository.

Here is an example of a Python .gitignore template provided by Gitlab.
### ![](assets/gitignorepython.png)

Would you like to know more? [.gitignore](https://git-scm.com/docs/gitignore)

## LICENSE

Gitlab provides the ability to make a **LICENSE**.md file. You want to make sure that you are giving credit where credit is due for the technologies that you are using within your Projects. 

Gitlab provides common licensing templates and makes them readily available for you. Simply add a new file and select LICENSE. Then select which type of Template you would like to apply.

Here is an example of an Apache License
### ![](assets/gitlablicense.png)

## CONTRIBUTE

Often times a repository will have a **CONTRIBUTE**.md file. If you come across a repository that you would like to contribute to or help on then you should definitely review this file. This file can contain information on contribution licensing, workflows, guidelines, templates, code of conduct, and all around rules on what you will need to do in order to submit changes and contribute to a repository or project properly. After reviewing the CONTRIBUTE file then you have permission to submit an issue to the repository.

Here is a sample **CONTRIBUTE**.md straight from Gitlab themselves.
[CONTRIBUTE](https://gitlab.com/gitlab-org/gitlab-recipes/-/blob/master/CONTRIBUTING.md)